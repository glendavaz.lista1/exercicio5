public class ContaBancaria
{
    // instance variables - replace the example below with your own
    private int numeroConta;
    private float saldo;    
    private float valorOperacao;
    
    public ContaBancaria()
    {
        
    }
    
    public double getNumeroConta() {
        return this.numeroConta;
    }
    
    public void setNumeroConta(int paramNumeroConta) {
        this.numeroConta = paramNumeroConta;
    }
    
    public double getSaldo() {
        return this.saldo;
    }
    
    public void setSaldo(float paramSaldo) {
        this.saldo = paramSaldo;
    }
    
    public float getValorOperacao() {
        return this.valorOperacao;
    }
    
    public void setValorOperacao(float paramValorOperacao) {
        this.valorOperacao = paramValorOperacao;
    } 
    
    public float realizaDeposito() {
        this.saldo = this.saldo + this.valorOperacao;
        return this.saldo;
    }
    
    public float realizaSaque() {
        this.saldo = this.saldo - this.valorOperacao;
        return this.saldo;
    }
    
}
