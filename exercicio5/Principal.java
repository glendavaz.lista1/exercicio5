import java.util.Scanner;
public class Principal
{
    public static void main (String[] args) {
        Scanner le = new Scanner(System.in);  
        ContaBancaria conta = new ContaBancaria();
        
        System.out.println("Insira o numero da conta:");
        conta.setNumeroConta(le.nextInt());
        
        System.out.println("Insira o saldo:");
        conta.setSaldo(le.nextFloat());
        
        System.out.println("Tipo de operaçao (1-Deposito e 2-Saque):");
        int tipoOperacao = le.nextInt();
        
        System.out.println("Insira o valor da operacao:");
        conta.setValorOperacao(le.nextFloat());
        
        if(tipoOperacao == 1) {
            conta.realizaDeposito();
        } else if(tipoOperacao == 2) {
            conta.realizaSaque();
        } else {
            System.out.println("Operaçao Invalida!");
        }
        
        System.out.println("Seu salto e: R$" + conta.getSaldo());
        
        
    }
  
}
